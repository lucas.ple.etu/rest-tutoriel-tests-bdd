package fr.ulille.iut.todo.ressource;

import java.net.URI;
import java.util.List;
import java.util.UUID;

import fr.ulille.iut.todo.dto.CreationTacheDTO;
import fr.ulille.iut.todo.service.Tache;
import fr.ulille.iut.todo.service.TodoService;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.EntityTag;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Request;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.ResponseBuilder;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.core.UriInfo;

@Path("taches")
@Produces(MediaType.APPLICATION_JSON)
public class TodoRessource {
    //private final static Logger LOGGER = Logger.getLogger(TodoRessource.class.getName());

    private TodoService todoService = new TodoService();

    @Context
    private UriInfo uri;
    @Context
    Request request;

    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<Tache> getAll() {
        //LOGGER.info("getAll()");

        return todoService.getAll();
    }

    @Path("{id}")
    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getTache(@PathParam("id") String id) {
    	Tache tache = todoService.getTache(UUID.fromString(id));
    	if(tache != null) {
    		return Response.ok(tache).build();
    	}
    	return Response.status(Status.NOT_FOUND).build();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createTache(CreationTacheDTO tacheDto) {
        //LOGGER.info("createTache()");

        Tache tache = Tache.fromCreationTacheDTO(tacheDto);
        todoService.addTache(tache);
        URI location = uri.getAbsolutePathBuilder().path(tache.getId().toString()).build();

        EntityTag etag = new EntityTag(Integer.toString(tache.hashCode()));
        ResponseBuilder builder = request.evaluatePreconditions(etag);

        if (builder == null) {
            builder = Response.created(location);
            builder.tag(etag);
            builder.entity(tache);
        }
        return builder.build();
    }
    
    @Path("{id}")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Tache putTache(@PathParam("id") String id, @QueryParam("nom") String nom, @QueryParam("description") String description) {
    	Tache nouvelle = new Tache();
    	nouvelle.setId(UUID.fromString(id));
    	nouvelle.setNom(nom);
    	nouvelle.setDescription(description);
    	todoService.updateTache(nouvelle);
    	return todoService.getTache(UUID.fromString(id));
    }
    
    @Path("{id}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteTache(@PathParam("id") String id) {
    	Tache t = todoService.getTache(UUID.fromString(id));
    	if(t == null) {
    		return Response.status(Status.NOT_FOUND).build();
    	}
    	todoService.deleteTache(id);
    	ResponseBuilder builder = Response.noContent();
    	return builder.build();
    }
    
    @Path("{id}/description")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response getDescription(@PathParam("id") String id) {
    	Tache t = todoService.getTache(UUID.fromString(id));
    	if(t == null) {
    		return Response.status(Status.NOT_FOUND).build();
    	}
    	return Response.ok(t.getDescription()).build();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response createTacheFormulaire(MultivaluedMap<String, String> formParams) {
    	Tache t = new Tache();
    	t.setNom(formParams.getFirst("nom"));
    	t.setDescription(formParams.getFirst("description"));
    	todoService.addTache(t);
    	Tache created = todoService.getTache(t.getId());
    	if(created == null) {
    		return Response.status(Status.NOT_FOUND).build();
    	}
    	URI location = uri.getAbsolutePathBuilder().path(t.getId().toString()).build();
    	ResponseBuilder rb = null;
    	rb = Response.created(location);
    	rb.entity(created);
    	return rb.build();
    }
}
